<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $guarded = [];

    public function rules()
    {
        return [
            'title' => 'string|max:255|unique:categories,title,' . $this->id,
            'parent_id' => 'nullable|exists:categories,id'
        ];
    }

    public function parent()
    {
        if ($this->hasParent()) {
            return $this->belongsTo(self::class);
        }

        return null;
    }

    public function hasParent()
    {
        if ($this->parent_id) {
            return true;
        }

        return false;
    }

    public function scopeParentId($query, $parentId)
    {
        return $query->where('parent_id', $parentId);
    }

    public function getParentTitle()
    {
        if ($this->hasParent()) {
            return $this->parent->title;
        }

        return null;
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function hasProducts()
    {
        return $this->products()->count() ? true : false;
    }
}
