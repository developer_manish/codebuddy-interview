<?php

namespace App\Http\Controllers;

use App\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{
    public function index()
    {
        $treeWiseCategories = CategoryService::getTreeWiseCategories(null);

        return view('dashboard.admin', compact('treeWiseCategories'));
    }
}
