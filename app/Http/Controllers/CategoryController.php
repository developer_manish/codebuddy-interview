<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('title')->paginate(20);

        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Validator::validate(request()->all(), (new Category)->rules());

        $category = Category::create($data);

        if ($category) {
            $request->session()->flash('success-message', 'Category added successfully!');
        } else {
            $request->session()->flash('error-message', 'Error occurred!');
        }

        return redirect()->route('dashboard.admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $data = Validator::validate(request()->all(), $category->rules());

        $category = $category->update($data);

        if ($category) {
            $request->session()->flash('success-message', 'Category updated successfully!');
        } else {
            $request->session()->flash('error-message', 'Error occurred!');
        }

        return redirect()->route('dashboard.admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $result = $category->delete();

        if ($result) {
            session()->flash('success-message', 'Category deleted successfully!');
        } else {
            session()->flash('error-message', 'Error occurred!');
        }

        return redirect()->route('dashboard.admin');
    }
}
