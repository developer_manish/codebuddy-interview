<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($category_id)
    {
        $category = Category::findOrFail($category_id);

        return view('products.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Validator::validate(request()->all(), (new Product)->rules());

        $product = Product::create($data);

        if ($product) {
            $request->session()->flash('success-message', 'Product added successfully!');
        } else {
            $request->session()->flash('error-message', 'Error occurred!');
        }

        return redirect()->route('dashboard.admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $data = Validator::validate(request()->all(), $product->rules());

        $product = $product->update($data);

        if ($product) {
            $request->session()->flash('success-message', 'Product updated successfully!');
        } else {
            $request->session()->flash('error-message', 'Error occurred!');
        }

        return redirect()->route('dashboard.admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $result = $product->delete();

        if ($result) {
            session()->flash('success-message', 'Product deleted successfully!');
        } else {
            session()->flash('error-message', 'Error occurred!');
        }

        return redirect()->route('dashboard.admin');
    }
}
