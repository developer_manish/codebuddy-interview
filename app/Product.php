<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function rules()
    {
        return [
            'title' => 'string|max:255|unique:products,title,' . $this->id,
            'category_id' => 'nullable|exists:categories,id'
        ];
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
