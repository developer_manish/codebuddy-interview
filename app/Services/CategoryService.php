<?php

namespace App\Services;

use App\Category;

class CategoryService
{
    public static function getTreeWiseCategories($parentId = null)
    {
        $resultArray = [];

        $rootCategories = Category::parentId($parentId)->orderBy('title')->get();

        foreach ($rootCategories as $rootCategory) {
            $resultArray[] = [
                'category' => $rootCategory,
                'children' => self::getTreeWiseCategories($rootCategory->id)
            ];
        }

        return $resultArray;
    }
}
