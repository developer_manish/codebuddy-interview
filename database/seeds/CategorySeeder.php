<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $root1 = Category::create([
            'title' => 'Root Category 1',
            'parent_id' => null
        ]);

        $level1_1 = Category::create([
            'title' => 'Level 1 - 1',
            'parent_id' => $root1->id
        ]);

        $level1_2 = Category::create([
            'title' => 'Level 1 - 2',
            'parent_id' => $root1->id
        ]);

        $level2_1 = Category::create([
            'title' => 'Level 2 - 1',
            'parent_id' => $level1_2->id
        ]);

        $root2 = Category::create([
            'title' => 'Root Category 2',
            'parent_id' => null
        ]);
    }
}
