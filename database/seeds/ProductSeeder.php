<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'title' => 'Product for Root Category 1',
            'category_id' => 1
        ]);

        Product::create([
            'title' => 'Product for Level 1 1',
            'category_id' => 2
        ]);
    }
}
