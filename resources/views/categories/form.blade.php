@isset($category)
{!! Form::model($category, ['url' => route('categories.update', $category), 'method' => 'put']) !!}
@else
{!! Form::open(['url' => route('categories.store')]) !!}
@endisset
<div class="row">

    <div class="col-md-12 form-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
        @error('title')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-12 form-group">
        {!! Form::label('parent_id', 'Parent Category') !!}
        {!! Form::select('parent_id', ['' => 'Select'] + \App\Category::where('id', '<>', $category->id ?? null)->orderBy('title')->pluck('title', 'id')->toArray(), old('parent_id'), ['class' => 'form-control']) !!}
        @error('parent_id')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-12 form-group">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
    </div>

</div>
{!! Form::close() !!}
