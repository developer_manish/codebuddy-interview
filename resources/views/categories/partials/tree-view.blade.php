<li class="mt-3">
    <span class="mr-5">
        {{ $categoryArr['category']->title }}
    </span>
    <span>
        <a href="{{ route('products.create', [$categoryArr['category']->id]) }}" class="btn btn-primary btn-sm">Add Product</a>

        <a href="{{ route('categories.edit', $categoryArr['category']) }}" class="btn btn-warning btn-sm">Edit</a>

        <a class="btn btn-danger btn-sm" href="{{ route('categories.destroy', $categoryArr['category']) }}" onclick="event.preventDefault();
                                                                document.getElementById('delete-form').submit();">
            {{ __('Delete') }}
        </a>

        <form id="delete-form" action="{{ route('categories.destroy', $categoryArr['category']) }}" method="POST" class="d-none">
            @csrf
            @method('DELETE')
        </form>

        @if ($categoryArr['category']->hasProducts())
        <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#products-{{ $categoryArr['category']->id }}" role="button" aria-expanded="false" aria-controls="collapseExample">
            View Products
        </a>
        @endif
    </span>

    <div class="collapse mt-3" id="products-{{ $categoryArr['category']->id }}">
        <div class="card card-body alert-info">
            <ul>
                @foreach ($categoryArr['category']->products as $product)
                    <li>{{ $product->title }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</li>
@if (count($categoryArr['children']))
<li class="ml-3">
    <ul>
        @foreach ($categoryArr['children'] as $child)
            @include('categories.partials.tree-view', ['categoryArr' => $child])
        @endforeach
    </ul>
</li>
@endif
