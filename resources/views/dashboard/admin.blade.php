@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="display: flex;justify-content: space-between">
                    {{ __('Categories') }}

                    <div class="">
                        <a href="{{ route('categories.create') }}" class="btn btn-success">Create</a>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <ul>
                                @forelse ($treeWiseCategories as $categoryArr)
                                    @include('categories.partials.tree-view', compact('treeWiseCategories'))
                                @empty
                                    <li>No records found..</li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
