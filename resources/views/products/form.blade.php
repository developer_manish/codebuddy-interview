@isset($product)
{!! Form::model($product, ['url' => route('products.update', [$category->id, $product]), 'method' => 'put']) !!}
@else
{!! Form::open(['url' => route('products.store', [$category->id])]) !!}
@endisset
<div class="row">

    <div class="col-md-12 form-group">
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
        @error('title')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

    {!! Form::hidden('category_id', $category->id) !!}

    <div class="col-md-12 form-group">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
    </div>

</div>
{!! Form::close() !!}
