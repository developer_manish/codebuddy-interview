<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserDashboardController;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middlware' => 'auth'], function() {

    Route::group(['prefix' => 'dashboard'], function() {
        Route::get('/admin', [AdminDashboardController::class, 'index'])->middleware('role.admin')->name('dashboard.admin');
        Route::get('/user', [UserDashboardController::class, 'index'])->name('dashboard.user');
    });

    Route::resource('categories', '\App\Http\Controllers\CategoryController')->middleware('role.admin')->except(['show']);

    Route::resource('{category_id}/products', '\App\Http\Controllers\ProductController')->middleware('role.admin')
        ->except(['index', 'edit', 'update', 'show', 'delete']);
});

